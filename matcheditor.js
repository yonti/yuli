var S_ALL, S_TOP;


'use strict';
var receivedList = [];
var DISPLAY_BB = {};
var imgList = [];
var QUERY_IMAGE_VISIBLE = true;
var NUMRECTS = 0;
var API = "https://extremeli.trendi.guru/api/yuli";
var IMAGE_NATURAL_WIDTH;
var IMAGE_NATURAL_HEIGHT;
var SELECTED_CATEGORY_ID = "";
var POST_DATA = {};
var IMAGEURL = '';
//TrendiGuru.POST_DATA = POST_DATA;
var SEARCH_RESULTS = [];
// Currently selected Bounding Box, initialize at -1
var BB_ID = -1;
var currPath="";
var svg_url="";
var svg = "";
var idx = "";

$(document).ready(function() {
    hideQueryImage();
});

$(window).load(function() {
    //link 'em up
    $('#submitUrlButton').click(classifyClicked);
    $('#searchButton').click(searchClicked);
    $('#input_url').focus(inputBoxFocused)
        .keypress(function(e) {
            if (e.which == 13) { //Enter key pressed
                event.preventDefault();
                classifyClicked();
            }
        });


    $('#input_url').val(getQueryVariable("imageURL"));
    classifyClicked();
});

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return ;
}


function classifyClicked() {
    $("#old_results").empty();
    $("#new_results").empty();
    //reset the question buttons
    $(".sub-question").remove();
    var IMAGEURL = $('#input_url').val();
    getQueryImage(IMAGEURL); // this calss getPost when its done
}

function getQueryImage(IMAGEURL) {
    var img = new Image();
    img.src = IMAGEURL;
    $(img).load(function() {
        if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth === 0) {
            //alert('broken image!');
        } else {
            IMAGE_NATURAL_WIDTH = img.width;
            IMAGE_NATURAL_HEIGHT = img.height;
            $("#query_image").attr('src', img.src);
            $("#query_with_results").slideDown("slow");
            QUERY_IMAGE_VISIBLE = true;

            getPost(IMAGEURL);
        }
    });
}

function getPost(IMAGEURL) {
    $.getJSON(API, {
            image_url: IMAGEURL
        })
        .done(function(data) {
            $("#floatingCirclesG").hide();
            gotPost(data,IMAGEURL);
        });
}


function gotPost(data,IMAGEURL) {
    POST_DATA = data;
    console.log(data)
    data.items.forEach(function(item) {
        displaySvg(item,IMAGEURL);
    });
}

function displaySvg(item,IMAGEURL) {
    var $container = $("#query_image_container");
    var img = $("#query_image")[0];
    var imgWidth = img.clientWidth;
    var imgHeight = img.clientHeight;
    var svg_url = item.svg_url
    var svg_id = item.category
    var theImage = new Image();
    theImage.src = $(img).attr("src");

    var widthScale = imgWidth / theImage.width;
    var heightScale = imgHeight / theImage.height;

    var trendiOverlay = $("#svg_overlay");
    // if trendiOverlay doesn't exist yet, create it
    if (trendiOverlay.length < 1) {
        trendiOverlay = $('<div></div>', {
            "class": "trendi trendiUX trendiOverlay",
            "id": "svg_overlay"
        });
        trendiOverlay.appendTo($container);
        trendiOverlay.width(imgWidth).height(imgHeight);
        trendiOverlay.load(svg_url, makeSVGCallBack($container, svg_url, svg_id, widthScale, heightScale,IMAGEURL,item));
    } else { //trendiOverlay exists, lets find the svg element and do cool stuff
        var svgLoader = $('<div></div>');
        svgLoader.load(svg_url, function(response, status, xhr) {
            var svg = $container.find("svg");
            var g = svgLoader.find("g");
            svg.append(g);
            //var currPath = $container.find("path");
            g.click(makePathClickedCallback(item,IMAGEURL));
        });
    }
}

//This creates the funciton that gets called when svg is done loading 
//Resize svg and path to match img size, make path clickable
function makeSVGCallBack(image_container, svg_url, svg_id, widthScale, heightScale, IMAGEURL,item) {
    return function(response, status, xhr) {
        if (status == "error") {
            console.log(xhr.status + " " + xhr.statusText);
        } else {
            //console.log("Successfully retreived SVG for: " + tg_Overlay);
        }

        var svg = image_container.find("svg");
        svg.attr("width", image_container.width());
        svg.attr("height", image_container.height());
        var currPath = image_container.find("path");
        currPath.click(makePathClickedCallback(item, IMAGEURL));

    };
}

function makePathClickedCallback(item,IMAGEURL) {
    return function(response, status, xhr) {
        if (status == "error") {
            console.log(xhr.status + " " + xhr.statusText);
        } else {

            //svg clicked!!!!
            //start spinner,
            $("#floatingCirclesG").show();
            //get results,
            $.getJSON(API, {
                    idx: item.idx.$oid,
                    weight: $('#mr8_weight').val(),
                    collection:"mr8_testing",
                    wing:"left",

                })
                .done(function(data_old) {
                    displaySearchResults(data_old, item.svg_url,0);
                });
            console.log("working on left side");   
            $.getJSON(API, {
                    idx: item.idx.$oid,
                    weight: $('#mr8_weight').val(),
                    collection:"mr8_testing",
                    wing:"right",

                })
                .done(function(data_new) {
                    $("#floatingCirclesG").hide();
                    displaySearchResults(data_new, item.svg_url,1);
                });
            //display resutls 
            console.log(item.svg_url + " clicked");

            //console.log("Successfully retreived SVG for: " + tg_Overlay);
        }
    };
}


function searchClicked(IMAGEURL) {
    //if this is a new post, create
    if (BB_ID === -1) {

    }
    $("#floatingCirclesG").show();
    var toSend = {
        url: IMAGEURL,
        bb: JSON.stringify(calculateTrueBoundingBox(DISPLAY_BB)),
        keyword: SELECTED_CATEGORY_ID,
        post_id: POST_DATA._id.$oid,
        item_id: BB_ID
    };
    console.log(JSON.stringify(toSend));
    $.getJSON(API, toSend)
        .done(function(data) {
            if (false) { // From when we wanted results only for Arden
                switchSSID(data.matches);
            }
            SEARCH_RESULTS = data.matches;
            $("#floatingCirclesG").hide();
            $("#old_results").empty();
            $("#new_results").empty();
            if ("matches" in data) {
                loadResults();
                displaySearchResults();
            }
            //scrollTo($("#results"));
        })
        .fail(function() {
            $("#floatingCirclesG").hide();
        });
}

function displaySearchResults(data, svg_url,side) {
    loadResults(data, svg_url,side);
    S_ALL = Sortable.create(results, {
        group: "results"
    });
    $("#sortable_container").show();
}

function inputBoxFocused(IMAGEURL) {
    //Here we more or less want to reset everything...
    $('#input_url').select();
    hideQueryImage();
    $("#sortable_container").hide();
    IMAGEURL='';
}


function hideQueryImage() {
    if (QUERY_IMAGE_VISIBLE) {
        $("#query_with_results").slideUp();
        QUERY_IMAGE_VISIBLE = false;
    }
}

function getItemsWithKey(listOfObjects, keyName, keyValue) {
    return $.grep(listOfObjects, function(e) {
        return e[keyName] == keyValue;
    });

}

function loadResults(data, svg_url, side) {
    // var item = getItemsWithKey(data.items, "svg_url", svg_url)[0];
    var $oldResults = $("#old_results");
    var $newResults = $("#new_results");
    
    if (side == 0){
        $oldResults.empty();
        data.similar_results.forEach(function(result_tuple) {
            var result_old = result_tuple;
            $oldResults.append(createResultDiv(result_old));
        }); 
    }
    else {
        $newResults.empty();
        data.similar_results.forEach(function(result_tuple) {
            var result_new = result_tuple;
            $newResults.append(createResultDiv(result_new));
    });   
    }
}

function createResultDiv(result) {
    var $rDiv = $('<div></div');
    
    if (result) {
        var imageURL = result['images']['XLarge'];
        $rDiv.addClass("col-lg-4 col-sm-4 col-xs-4 resultImage")
            .append('<a href="' + result.clickUrl + '" target="_blank" class="thumbnail"><img src="' + imageURL + '" class="img-responsive"></a>')
            .attr("data-id", result.id);
    }
    return $rDiv;
}

function getDictFromListByKey(listOfDicts, key) {
    var dbk = {};
    for (var i = 0; i < listOfDicts.length; i++) {
        dbk[listOfDicts[i][key]] = listOfDicts[i];
    }
    return dbk;
}


/*
Utility functions
*/
function getSearchResultIndex(resultId) {
    for (var i = 0; i < SEARCH_RESULTS.length; i++) {
        if (SEARCH_RESULTS[i].id == resultId) {
            return i;
        }
    }
    return -1;
}

function calculateTrueBoundingBox(displayBB) {
    var imgScale = getImgScale();

    //bb = [x, y, w, h]
    var trueBB = [
        displayBB[0] / imgScale.widthScale,
        displayBB[1] / imgScale.heightScale,
        displayBB[2] / imgScale.widthScale,
        displayBB[3] / imgScale.heightScale
    ];

    return trueBB;
}

function calculateDisplayBoundingBox(trueBB) {
    var imgScale = getImgScale();

    //bb = [x, y, w, h]
    var displayBB = [
        trueBB[0] * imgScale.widthScale,
        trueBB[1] * imgScale.heightScale,
        trueBB[2] * imgScale.widthScale,
        trueBB[3] * imgScale.heightScale
    ];

    return displayBB;
}

/**
 * calculate's query_image's scale.
 * The important thing to not is that natural image height and width are saved on load
 * @return { widthScale: number,
 *           heightScale: number}
 */
function getImgScale() {
    var img = $("#query_image")[0];
    return {
        widthScale: img.clientWidth / IMAGE_NATURAL_WIDTH,
        heightScale: img.clientHeight / IMAGE_NATURAL_HEIGHT
    };
}

/*
function scrollTo(jQueryElement) {
// Scroll
$('html,body').animate({
    scrollTop: jQueryElement.offset().top
}, 'slow');
}
*/

function getShopStyleItemIdFromUrl(url) {
    return /id=([0-9]*)/.exec(url)[1];
}

function getShopStylePidFromUrl(url) {
    return /pid=([^&]*)&/.exec(url)[1];
}




//------- End TrendiGuru namespace ------------//
//
//
//
//
//
